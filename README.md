Configuration and scripts so that 'rm'ing a file from the CLI uses trash-put instead, putting it into the XDG Trash system.


TODO
====

1. Make a singular idempotent install file/script/Makefile/whatever.
1. Create usage directions.
1. Create a git hook to run the idempotent install file whenever I make a commit to the master branch!

